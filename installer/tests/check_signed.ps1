$paths = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\MediaPlayer","C:\Program Files\Streambox\Mediaplayer" 
$manifest = Get-ChildItem -Recurse $paths | Select-Object -Expand Fullname
$manifest
$check_signed = $manifest | Where-Object {$_ -notmatch '.*\.(txt|bmp|lnk)$'} # these can't be signed
signtool verify /v /pa $check_signed
