function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}




$version = 'latest'

cd C:\Windows\temp
$url = "https://streambox-mediaplayer.s3.us-west-2.amazonaws.com/${version}/win/mediaplayer.zip"
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename
$global:ProgressPreference = "SilentlyContinue"
If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename))){ Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename}

&"$($fileinfo.Basename)/mediaplayer.exe" /install /passive /log "mediaplayer_install_${version}.log" | Out-String

$installers = Get-ChildItem -Recurse "C:\ProgramData\Package Cache" -ea 0 | Select -Expand Fullname | Select-String -Pattern "mediaplayer.exe"
$installers












$version = '1.12.0.1'

cd C:\Windows\temp
$url = "https://streambox-mediaplayer.s3.us-west-2.amazonaws.com/win/${version}/mediaplayer_all_${version}.zip"
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename
$global:ProgressPreference = "SilentlyContinue"
If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename))){ Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename}

&"$($fileinfo.Basename)/8bit/mediaplayer.exe" /install /passive /log "mediaplayer_install_${version}.log" | Out-String


$installers = Get-ChildItem -Recurse "C:\ProgramData\Package Cache" -ea 0 | Select -Expand Fullname | Select-String -Pattern "mediaplayer.exe"
$installers
