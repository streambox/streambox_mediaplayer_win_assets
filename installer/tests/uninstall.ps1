$installers = Get-ChildItem -Recurse "C:\ProgramData\Package Cache" -ea 0 | Select -Expand Fullname | Select-String -Pattern "mediaplayer.exe"
$installers

for ($c=1; $c -lt $installers.Length+1; $c++){
    $installer = $installers[$c-1]
    $fileinfo = New-Object System.IO.FileInfo($installer)
    $log = "$($fileinfo.Basename)_uninstall_${c}.log"
    Write-Host $installer
    Write-Host $log
    &$installer /uninstall /passive /log $log | Out-String
}
