function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = '1.18.1.0'
$bit = '8bit'
$bit = '10bit'

cd C:\Windows\temp
$url = "https://streambox-mediaplayer.s3.us-west-2.amazonaws.com/win/${version}/mediaplayer_all_${version}.zip"
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename
$global:ProgressPreference = "SilentlyContinue"
If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename))){ Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename}

&"$($fileinfo.Basename)/${bit}/mediaplayer.exe" /install /passive /log "mediaplayer_install_${version}.log" | Out-String
