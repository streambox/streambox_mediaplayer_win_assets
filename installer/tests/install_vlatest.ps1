function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

cd C:\Windows\temp
$url = "https://streambox-mediaplayer.s3.us-west-2.amazonaws.com/latest/win/mediaplayer.zip"
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
$newdir = "${pwd}/mediaplayer_latest"
New-Item -Type "directory" -Force -Path $newdir | Out-Null
$filename1 = "$newdir/$filename"
If(Test-Path $filename1){ Remove-Item $filename1 } # for latest I want to always downlaod since filename is not unique amongst versions
Get-FileIfNotExists $url $filename1
$global:ProgressPreference = "SilentlyContinue"
If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename1))){ Expand-Archive -Force $filename1 -DestinationPath $newdir}
$installer = Get-ChildItem -Recurse . -Filter "mediaplayer.exe" | Select-Object -Expand Fullname

&$installer /install /passive /log "mediaplayer_install_latest.log" | Out-String
