param
(
    [string]$command = "Add"
)

Function Remove-FW() {
    &C:\Windows\System32\netsh.exe advfirewall firewall `
      delete rule name="Streambox Mediplayer"
}

Function Add-FW() {
    Remove-FW # prevent duplicates
    &C:\Windows\System32\netsh.exe advfirewall firewall add rule `
      name="Streambox Mediplayer" dir=in action=allow `
      program="C:\Program Files\Streambox\Mediaplayer\SMPlayer.exe" `
      enable=yes profile=Any
}

switch($command){
    "Add" { Add-FW }
    "Remove" { Remove-FW }
}
